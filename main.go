package main

import (
	"context"
	"fmt"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"github.com/spf13/viper"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"google.golang.org/grpc"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"net"
	"user/pb"
)

type Config struct {
	Address string `mapstructure:"address"`

	Postgresql struct {
		Host     string `mapstructure:"host"`
		Port     string `mapstructure:"port"`
		User     string `mapstructure:"user"`
		DbName   string `mapstructure:"db_name"`
		Password string `mapstructure:"password"`
	} `mapstructure:"postgresql"`
}

func LoadConfig() error {
	viper.SetConfigFile("configs/config.yaml")
	viper.AutomaticEnv()

	err := viper.ReadInConfig()
	if err != nil {
		return err
	}

	err = viper.Unmarshal(&common)

	return nil
}

var common *Config
var db *gorm.DB

type User struct {
	Id   uint64
	Name string
	Age  uint32
}

func main() {
	err := LoadConfig()
	if err != nil {
		panic(err)
	}
	cf := common.Postgresql
	dsn := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s", cf.Host,
		cf.Port, cf.User, cf.DbName, cf.Password)
	db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	_ = db.AutoMigrate(User{})

	server := grpc.NewServer(
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			otelgrpc.UnaryServerInterceptor(),
			grpc_recovery.UnaryServerInterceptor(), // recover from gRPC handler panics into a gRPC error with `code.Internal`
		)),
	)
	lis, err := net.Listen("tcp", common.Address)
	if err != nil {
		panic(err)
	}

	userHandler := &UserHandler{}
	pb.RegisterUserServiceServer(server, userHandler)

	if err = server.Serve(lis); err != nil {
		log.Fatal("Listen GRPC Server error", err)
	}

}

type UserHandler struct {
	pb.UnimplementedUserServiceServer
}

func (u *UserHandler) Create(c context.Context, req *pb.CreateUserRequest) (*pb.User, error) {
	log.Print("receive request")
	user := &User{
		Name: req.Name,
		Age:  req.Age,
	}
	err := db.WithContext(c).Save(user).Error
	if err != nil {
		return nil, err
	}
	return &pb.User{
		Id:   user.Id,
		Name: user.Name,
		Age:  user.Age,
	}, nil
}
