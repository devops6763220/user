tidy:
	go mod tidy

path_proto:=./pb
proto:
	protoc --go_out=${path_proto} --go-grpc_out=${path_proto} ${path_proto}/*.proto


namespace := devops

helm-install:
	kubectl create namespace ${namespace}
	kubectl create configmap user-config --from-file=configs/config.yaml --namespace ${namespace}
	helm install user deployment --namespace ${namespace}
helm-upgrade:
	helm upgrade user deployment --namespace ${namespace}

