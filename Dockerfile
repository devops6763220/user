#Build
FROM golang:1.20-alpine AS builder

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . .
ENV CGO_ENABLED=0 \
    GO111MODULE=on

RUN go build -o user

# Runtime
FROM alpine:3.5
WORKDIR /app

COPY --from=builder /app/user ./app-user

ENTRYPOINT ["/app/app-user"]